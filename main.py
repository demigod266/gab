import os
import random
import sys
import threading
import time
import zipfile
from datetime import date
from selenium.webdriver.common.keys import Keys
from fake_useragent import UserAgent
import eel
from selenium import webdriver
import requests
global driver
global PROXY_HOST
global PROXY_PORT
global PROXY_USER
global PROXY_PASS
global printLog
thread_running = True
LogDataList= []

def licenseValidtion():
    printingLog(msg="Validating License")

    try:
        URL = "https://hamzashakeelbwp.blogspot.com/2021/01/gabpython.html"
        r = requests.get(url=URL)
        data = r.text
        if 'gabpython' in data:
            print("Success")
        else:
            try:
                driver.quit()
                sys.exit()
            except:
                print("No Driver")
            exit(1)
            sys.exit()
    except:
        try:
            driver.quit()
            sys.exit()
        except:
            print("No Driver")

        exit(1)
        sys.exit()





eel.init('web')



@eel.expose
def createMultipleThreads(threads,botmode,proxymode,min,max,userAgent,resolution):
    thread_running = True
    if (thread_running):
        if (botmode=="clickspecific"):
            printingLog(msg="Running Bot in Click Specific Site Mode")
            for i in range(0,int(threads)):
                y = threading.Thread(target=Clickspecific,args=(min,max,proxymode,userAgent,resolution))
                y.start()
                time.sleep(10)
        else:
            printingLog(msg="Running Bot in All Except Mode")
            for i in range(0,int(threads)):
                y = threading.Thread(target=clickall,args=(min,max,proxymode,userAgent,resolution))
                y.start()
                time.sleep(10)


def get_proxy():
    printingLog(msg="Getting Proxy Hosts")
    appDataFolder = os.getcwd()
    appDataFolderProxies = appDataFolder + "\\Settings\\proxies.txt"

    try:
        f = open(appDataFolderProxies, "r")
        dataRaw = f.read()
        dataProxy = dataRaw.split("\n")

    except Exception as e:
        print("no website Present")
    dataProxyRand = random.choice(dataProxy)
    dataProxySelected = dataProxyRand.split(":")

    return dataProxySelected

def get_chromedriver(use_proxy=True,userAgent="chrome",resolution="1920X1080"):
    selectedProxy = get_proxy()
    PROXY_HOST = selectedProxy[0]
    PROXY_PORT = selectedProxy[1]
    PROXY_USER = selectedProxy[2]
    PROXY_PASS = selectedProxy[3]
    ua = UserAgent(cache=False)

    manifest_json = """
    {
        "version": "1.0.0",
        "manifest_version": 2,
        "name": "Chrome Proxy",
        "permissions": [
            "proxy",
            "tabs",
            "unlimitedStorage",
            "storage",
            "<all_urls>",
            "webRequest",
            "webRequestBlocking"
        ],
        "background": {
            "scripts": ["background.js"]
        },
        "minimum_chrome_version":"22.0.0"
    }
    """

    background_js = """
    var config = {
            mode: "fixed_servers",
            rules: {
              singleProxy: {
                scheme: "http",
                host: "%s",
                port: parseInt(%s)
              },
              bypassList: ["localhost"]
            }
          };

    chrome.proxy.settings.set({value: config, scope: "regular"}, function() {});

    function callbackFn(details) {
        return {
            authCredentials: {
                username: "%s",
                password: "%s"
            }
        };
    }

    chrome.webRequest.onAuthRequired.addListener(
                callbackFn,
                {urls: ["<all_urls>"]},
                ['blocking']
    );
    """ % (PROXY_HOST, PROXY_PORT, PROXY_USER, PROXY_PASS)

    path = os.path.dirname(os.path.abspath(__file__))
    chrome_options = webdriver.ChromeOptions()
    if use_proxy:
        pluginfile = 'proxy_auth_plugin.zip'

        with zipfile.ZipFile(pluginfile, 'w') as zp:
            zp.writestr("manifest.json", manifest_json)
            zp.writestr("background.js", background_js)
        chrome_options.add_extension(pluginfile)
    # chrome_options.add_argument("--incognito")

    appDataFolder = os.getcwd()
    if (userAgent == "chrome"):
        appDataFolderUserAgent = appDataFolder + "\\Settings\\chromeUserAgents.txt"
    elif userAgent == "firefox":
        appDataFolderUserAgent = appDataFolder + "\\Settings\\firefoxUserAgents.txt"
    elif userAgent == "safari":
        appDataFolderUserAgent = appDataFolder + "\\Settings\\safariUserAgents.txt"
    elif userAgent == "andriod":
        appDataFolderUserAgent = appDataFolder + "\\Settings\\andriodUserAgents.txt"
    elif userAgent == "iphone":
        appDataFolderUserAgent = appDataFolder + "\\Settings\\iphoneUserAgents.txt"
    try:
        f = open(appDataFolderUserAgent, "r")
        dataRawUserAgents = f.read()
        dataUserAgents = dataRawUserAgents.split("\n")
    except Exception as e:
        print("no useragent Present")
    userAgentSelected =random.choice(dataUserAgents)

    dataresolution = resolution.split("X")

    if (userAgentSelected!=""):
        chrome_options.add_argument('--user-agent=%s' % userAgentSelected)
    else:
        chrome_options.add_argument('--user-agent=%s' % ua.chrome)
    printingLog(msg="Launching WebBrowser")
    driver = webdriver.Chrome(
        executable_path="chromedriver/chromedriver.exe",
        options=chrome_options)
    driver.set_window_size(dataresolution[0], dataresolution[1])

    return driver

@eel.expose
def Clickspecific(minT,maxT,proxy,userAgent,resolution):
    minT = int(minT)
    maxT = int(maxT)
    thread_running = True
    if(proxy=="proxy"):
        setProxy = True
    else:
        setProxy = False


    if (sys.platform == "win32"):
        appDataFolder = os.getcwd()
        appDataFolderIncludeFile = appDataFolder + "\\Settings\\includewebsitelist.txt"
        appDataFolderTerms = appDataFolder + "\\Settings\\terms.txt"

    try:
        f = open(appDataFolderIncludeFile, "r")
        dataRaw = f.read()
        dataIncludeFile = dataRaw.split("\n")
    except Exception as e:
        print("no website Present")

    try:
        f = open(appDataFolderTerms, "r")
        dataRaw = f.read()
        dataTerms = dataRaw.split("\n")
    except Exception as e:
        print("No Term Present")

    for i in range(0,len(dataTerms)):
        if(thread_running):
            try:
                driver = get_chromedriver(use_proxy=setProxy,userAgent=userAgent,resolution=resolution)
            except:
                driver.quit()
                driver = get_chromedriver(use_proxy=setProxy,userAgent=userAgent,resolution=resolution)
            selectedDataTerm = dataTerms[i]
            includeWebsiteFunction(driver, "https://www.google.com", minT, maxT,selectedDataTerm,dataIncludeFile,proxy,userAgent,resolution)
        else:
            driver.quit()
            break

@eel.expose
def getAllResolutions():
    appDataFolder = os.getcwd()
    appDataFolderResolution = appDataFolder + "\\Settings\\resolutions.txt"
    try:
        f = open(appDataFolderResolution, "r")
        dataRawResolutions = f.read()
        dataResolutions = dataRawResolutions.split("\n")
    except Exception as e:
        print("no website Present")
    return dataResolutions

def humanTyping(text,element):
    for char in text:
        time.sleep(random.randint(1, 4))
        element.send_keys(char)



@eel.expose
def clickall(minT,maxT,proxy,userAgent,resolution):
    minT = int(minT)
    maxT = int(maxT)
    thread_running = True
    if(proxy=="proxy"):
        setProxy = True
    else:
        setProxy = False


    if (sys.platform == "win32"):
        appDataFolder = os.getcwd()
        appDataFolderExcludeFile = appDataFolder + "\\Settings\\excludeWebsiteList.txt"
        appDataFolderTerms = appDataFolder + "\\Settings\\terms.txt"

    try:
        f = open(appDataFolderExcludeFile, "r")
        dataRaw = f.read()
        dataExcludeFile = dataRaw.split("\n")
    except Exception as e:
        print("no website Present")

    try:
        f = open(appDataFolderTerms, "r")
        dataRaw = f.read()
        dataTerms = dataRaw.split("\n")
    except Exception as e:
        print("No Term Present")

    for i in range(0,len(dataTerms)):
        if(thread_running):
            try:
                driver = get_chromedriver(use_proxy=setProxy,userAgent=userAgent,resolution=resolution)
            except:
                driver.quit()
                driver = get_chromedriver(use_proxy=setProxy,userAgent=userAgent,resolution=resolution)
            selectedDataTerm = dataTerms[i]
            excludeWebsiteFunction(driver, "https://www.google.com", minT, maxT,selectedDataTerm,dataExcludeFile,proxy,userAgent,resolution)
        else:
            driver.quit()
            break

def excludeWebsiteFunction(driver,google,minT,maxT,dataTerms,dataExcludeFile,proxy,userAgent,resolution):
    if (thread_running):
        driver.get(google)
        time.sleep(10)
        selectedTerm = dataTerms
        printingLog(msg="Entering the Term")

        try:
            element = driver.find_element_by_xpath("//input[@name='q']")
            humanTyping(selectedTerm,element)
        except:
            printingLog(msg="Wrong User Agent")
            driver.quit()
            clickall(minT, maxT, proxy,userAgent,resolution)
        time.sleep(5)

        try:
            driver.find_element_by_xpath("//input[@name='q']").send_keys(Keys.ENTER)
        except:
            print("cannot press enter")
        try:
            driver.find_element_by_xpath("//div[contains(@data-async-context,'query')]")
        except:
            print("cannot press enter")
        removeNonAds(driver)
        try:
            totalAds = driver.find_elements_by_xpath("//div[contains(@data-hveid,'QAA')]")
            totalAds = len(totalAds)
        except:
            totalAds=0

        if totalAds > 0:
            printingLog(msg="Ads Found")

            for i in range(1,totalAds+1):
                if (thread_running):
                    currentElementXpath = "(//div[contains(@data-hveid,'QAA')]/div/div/a)["+str(i)+"]"
                    currentElement = driver.find_element_by_xpath(currentElementXpath).get_attribute("href")
                    if "aclk" in currentElement:
                        continue
                    currentElementhveidXpath= "(//div[contains(@data-hveid,'QAA')]/div/div/a)["+str(i)+"]/parent::div/parent::div/parent::div"
                    currentElementhveid = driver.find_element_by_xpath(currentElementhveidXpath).get_attribute("data-hveid")
                    for website in dataExcludeFile:
                        currentWebsiteClicked = website
                        if website in currentElement:
                            isWebsitePresent = True
                            break
                        else:
                            isWebsitePresent = False

                    if(isWebsitePresent==False):
                        printingLog(msg="Clicking the Website")

                        currentWebsiteClickXpath = "//div[@data-hveid='"+str(currentElementhveid)+"']/div/div/a"
                        driver.find_element_by_xpath(currentWebsiteClickXpath).click()
                        try:
                            randomWaitTime= getRandomTime(minT,maxT)
                        except:
                            randomWaitTime = 20
                        scrollandClick(driver, randomWaitTime, currentWebsiteClicked)
                        time.sleep(randomWaitTime)
                        navigateBack(driver)
                else:
                    driver.quit()
                    break
        driver.quit()
    else:
        driver.quit()
        exit()

def includeWebsiteFunction(driver,google,minT,maxT,dataTerms,dataIncludeFile,proxy,userAgent,resolution):
    if (thread_running):
        driver.get(google)
        time.sleep(10)
        selectedTerm = dataTerms
        printingLog(msg="Entering the Term")
        try:
            element=driver.find_element_by_xpath("//input[@name='q']")
            humanTyping(selectedTerm,element)

        except:
            driver.quit()
            Clickspecific(minT, maxT, proxy,userAgent,resolution)
        time.sleep(5)

        try:
            driver.find_element_by_xpath("//input[@name='q']").send_keys(Keys.ENTER)
        except:
            print("cannot press enter")
        try:
            driver.find_element_by_xpath("//div[contains(@data-async-context,'query')]")
        except:
            print("cannot press enter")
        removeNonAds(driver)
        try:
            totalAds = driver.find_elements_by_xpath("//div[contains(@data-hveid,'QAA')]")
            totalAds = len(totalAds)
        except:
            totalAds=0

        if totalAds > 0:
            printingLog(msg="Ads Found")

            for i in range(1,totalAds+1):
                if (thread_running):
                    currentElementXpath = "(//div[contains(@data-hveid,'QAA')]/div/div/a)["+str(i)+"]"
                    currentElement = driver.find_element_by_xpath(currentElementXpath).get_attribute("href")
                    if "aclk" in currentElement:
                        continue
                    currentElementhveidXpath= "(//div[contains(@data-hveid,'QAA')]/div/div/a)["+str(i)+"]/parent::div/parent::div/parent::div"
                    currentElementhveid = driver.find_element_by_xpath(currentElementhveidXpath).get_attribute("data-hveid")
                    for website in dataIncludeFile:
                        currentWebsiteClicked = website
                        if website in currentElement:
                            isWebsitePresent = True
                            break
                        else:
                            isWebsitePresent = False

                    if(isWebsitePresent==True):
                        printingLog(msg="Clicking the Website")

                        currentWebsiteClickXpath = "//div[@data-hveid='"+str(currentElementhveid)+"']/div/div/a"
                        driver.find_element_by_xpath(currentWebsiteClickXpath).click()
                        try:
                            randomWaitTime = getRandomTime(minT, maxT)
                        except:
                            randomWaitTime = 10
                        printingLog(msg="Waiting on Website")
                        scrollandClick(driver, randomWaitTime, currentWebsiteClicked)
                        time.sleep(randomWaitTime)
                        navigateBack(driver)
                else:
                    driver.quit()
                    break
        driver.quit()
    else:
        driver.quit()
        exit()

def navigateBack(driver):
    driver.execute_script("window.history.go(-1)")
    removeNonAds(driver)

def getRandomTime(min,max):
    randomWaitTime = random.randint(min, max)
    return randomWaitTime


def removeNonAds(driver):
    javaScript = """var nodes =document.evaluate("//div[contains(@data-async-context,'query')]", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null)
        var parentnode=nodes.singleNodeValue.id
        document.getElementById(parentnode).remove()
        var nodes =document.getElementById("botstuff").remove()"""

    driver.execute_script(javaScript)
    time.sleep(10)
@eel.expose
def stopThreads():
    printingLog(msg="Stopping Bot")
    global thread_running
    thread_running=False
    print("Bot Closed")
    sys.exit()

# def main():
#     notClickMainFunction(10,20,"none")
#     time.sleep(10)
#
# if __name__ == '__main__':
#     main()

def licenseValidtion():
    printingLog(msg="Validating License")

    try:
        URL = "https://hamzashakeelbwp.blogspot.com/2021/01/gabpython.html"
        r = requests.get(url=URL)
        data = r.text
        if 'gabpython' in data:
            print("Success")
        else:
            try:
                driver.quit()
                sys.exit()
            except:
                print("No Driver")
            exit(1)
            sys.exit()
    except:
        try:
            driver.quit()
            sys.exit()
        except:
            print("No Driver")

        exit(1)
        sys.exit()


def printingLog(**kwargs):
    if (thread_running):
        currentdate = date.today()
        if (len(kwargs) == 1):
            LogData = str(currentdate) + ": " + str(kwargs.get('msg', None))
            print(LogData)
            LogDataList.append(LogData)
        elif (len(kwargs) == 2):
            LogData = str(currentdate) + ": " + str(kwargs.get('msg', None)) + ": " + str(kwargs.get('var', None))
            print(LogData)
            LogDataList.append(LogData)

        if (sys.platform == "win32"):
            appDataFolder = os.getcwd()
            appDataFolder = appDataFolder + "\\Settings\\outputLogs.txt"
        else:
            appDataFolder = os.getcwd()
            appDataFolder = appDataFolder + "/Settings/outputLogs.txt"
            isFlagFile = True
        try:
            f = open(appDataFolder, "x")
        except Exception as e:
            isFlagFile = False

        with open(appDataFolder, 'w') as f:
            for item in LogDataList:
                f.write("%s\n" % item)


def scrollandClick(driver,randomWaitTime,currentWebsiteClicked):
    try:
        driver.execute_script("window.scrollBy(0,350)")
        time.sleep(random.randint(1,randomWaitTime-2))
        driver.execute_script("window.scrollBy(0,350)")
        time.sleep(random.randint(1,randomWaitTime-2))
        driver.execute_script("window.scrollBy(0,-350)")
        time.sleep(random.randint(1,randomWaitTime-2))
        xpathToCLickLink = "(//a[contains(@href,'"+currentWebsiteClicked+"')])[1]"
        driver.find_element_by_xpath("""(//a[@href])[3]""").click()
        time.sleep(4)
        navigateBack(driver)
    except:
        print("Cannot Scroll")


printingLog(msg="Bot is Running")
licenseValidtion()
eel.start('dist/index.html', size=(1080, 680))